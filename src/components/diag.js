import React from 'react'
import ReactFlow from 'react-flow-renderer';


const Diag = () => {
  const elements = [

    // default node
    {
      id: '2',
      data: { label: 'Transicope en Pays Nantais' },
      position: { x: 100, y: 0 },
      sourcePosition: 'left',
      style: {
        height: 50,
        width: 300,
        background : 'silver'
      },
    },
    {
      id: '3',
      // type: 'output', // output node
      data: { label: 'Autre Agenda Nantais' },
      position: { x: 100, y: 550 },
      targetPosition: 'left',
      style: {
        height: 100,
      },
    },
    {
      id: '4',
      // type: 'output', // output node
      data: { label:<a href="./ian"><div>Interconnexion des alternatives en pays nantais</div></a>},
      position: { x: 100, y: 150 },
      targetPosition: 'left',
      sourcePosition: 'right',
      style: {
        height: 100,
      },
    },
    {
      id: '6',
      data: { label: 'Livre Blanc pour le climat' },
      position: { x: 450, y: 0 },
      sourcePosition: 'right',
      style: {
        height: 50,
        width: 300,
        background : 'silver'
      },
    },
    {
      id: '7',
      data: { label: 'Atelier citoyen' },
      position: { x: 600, y: 150 },
      targetPosition: 'right',
      sourcePosition :'left',
      style: {
        height: 100,
      },
    },
    {
      id: '8',
      data: { label: 'Baromètre' },
      position: { x: 600, y: 300 },
      targetPosition: 'right',
      style: {
        height: 100,
      },
    },
    {
      id: '9',
      data: { label: 'Livre Blanc' },
      targetPosition: 'right',
      position: { x: 600, y: 450 },
      style: {
        height: 100,
      },
    },
    {
      id: '1',
      // type: 'input', // input node
      data: { label: 'Interconnexions et Climat' },
      position: { x: 300, y: 100 },
      targetPosition: 'left',
      sourcePosition: 'right',
      style: {
        width: 250,
        height: 300,
        background : 'green'
      }
    },
    // animated edge
    { id: 'e2-3', source: '2', target: '3', type: 'step' },
    { id: 'e2-4', source: '2', target: '4', type: 'step' },
    { id: 'e4-1', source: '4', target: '1'},
    { id: 'e7-1', source: '1', target: '7'},
    { id: 'e7-1', source: '1', target: '8'},
    { id: 'e6-7', source: '6', target: '7', type: 'step' },
    { id: 'e6-8', source: '6', target: '8', type: 'step' },
    { id: 'e6-8', source: '6', target: '9', type: 'step' },
  ];

  return (
    <div style={{ height: 600 }}>
      <ReactFlow elements={elements} />
    </div>
  )
}
export default Diag
