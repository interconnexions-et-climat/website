import React, {useRef, useEffect} from 'react';
import { navigate } from "gatsby"
import Diagram, {
  Nodes,
  Edges,
  AutoLayout,
  Toolbox,
  Group,
  MainToolbar,
  ViewToolbar,
  HistoryToolbar,
  PropertiesPanel,
  Editing,
  GridSize,
  ContextToolbox
} from 'devextreme-react/diagram';
import ArrayStore from 'devextreme/data/array_store';
import {Helmet} from "react-helmet";

const Diag = () => {

  const target = useRef(null);

  const flowNodes = [
    {
      id: 12,
      text: `Interconnexion des alternatives en pays nantais`,
      url: '/ian',
      left: 0,
      top: 2.125,
      width: 2,
      kind: "chantier"
    }, {
      id: 14,
      text: 'Ateliers Citoyens',
      url: '/ateliers-citoyens',
      left: 8,
      top: 1.125,
      width: 2,
      kind: "chantier"
    }, {
      id: 15,
      text: 'Baromètre',
      url: '/barometre',
      left: 8,
      top: 3.125,
      width: 2,
      kind: "chantier"
    }, {
      id: 50,
      url: '/Campagne',
      top: 0.75,
      left: 2.5,
      height: 3.5,
      width: 5,
      type: "ellipse",
      kind: "campagne",
      // childKeys: [ 20 ]
    }, {
      id: 20,
      top: 2,
      left: 3,
      width: 2,
      height: 1,
      type: "ellipse",
      text: 'renforcer les réseaux des alternatives'
    }, {
      id: 21,
      top: 1,
      left: 4.5,
      width: 2,
      height: 1,
      type: "ellipse",
      text: 'coconstruire les politiques publiques'
    }, {
      id: 22,
      top: 3,
      left: 4.5,
      width: 2,
      height: 1,
      type: "ellipse",
      text: `mesurer l'impact des politiques publiques`
    }
  ];
  const flowEdges = [
    {
      id: 106,
      fromId: 12,
      toId: 20,
      toLineEnd: "arrow"
    }, {
      id: 107,
      fromId: 14,
      toId: 21,
      toLineEnd: "arrow"
    }, {
      id: 108,
      fromId: 15,
      toId: 22,
      toLineEnd: "arrow"
    }, {
      id: 109,
      fromId: 20,
      toId: 21,
      type: "straight",
      toLineEnd: "outlinedTriangle",
      fromLineEnd: "outlinedTriangle",
      fromPointIndex: 0
    }, {
      id: 110,
      fromId: 22,
      toId: 21,
      toLineEnd: "outlinedTriangle",
      type: "straight"
    }, {
      id: 111,
      fromId: 22,
      toId: 20,
      type: "straight",
      toLineEnd: "outlinedTriangle",
      toPointIndex: 2
    }
  ];

  const flowNodesDataSource = new ArrayStore({key: 'id', data: flowNodes});
  const flowEdgesDataSource = new ArrayStore({key: 'id', data: flowEdges});

  const style = (el) => {
    let style = {};
    switch (el.kind) {
      case "collectif":
        style = {
          ...style,
          fill: "mediumseagreen"
        }
        break;
      case "chantier":
        style = {
          ...style,
          fill: "silver"
        }
        break;
      case "campagne":
        style = {
          ...style,
          fill: "teal"
        }
        break;
      default:

    }
    // console.log('style',style);
    return style;
  }

  const styleText = (el) => {
    // console.log('ALLLLO');
    let style = {};
    switch (el.kind) {
      case "campagne":
        style = {
          ...style,
          "fill": "white"
        }
        break;
      default:
    }
    return style;
  }

  // const navigate = (el) => {
  //   if (el.item.dataItem.url) {
  //     window.location.href = el.item.dataItem.url;
  //   }
  // }

  const fitContent = (el) => {
    setTimeout(() => {


      const dxiMain = el.element.querySelector('.dxdi-main');

      dxiMain.transform.baseVal.getItem(0).setTranslate(dxiMain.transform.baseVal.getItem(0).matrix.e, 0);



      const height = dxiMain.getBBox().height

      // console.log('height',height);

      const firstG = dxiMain.firstChild;
      // const scale = firstG.transform.baseVal.getItem(0).matrix.a;
      // console.log('scale',scale);

      const dxiCanvas = el.element.querySelector('.dxdi-canvas');
      dxiCanvas.removeAttribute('viewBox');
      dxiCanvas.style.height=height/0.8;
      const clone = dxiCanvas.cloneNode(true);
      const shapes = clone.querySelectorAll('.shape');
      shapes.forEach((item, i) => {
        item.style.cursor='auto'
      });
      flowNodes.forEach((item, i) => {
        const shape = shapes[flowNodes.length+i];
        if(item.url){
          shape.style.cursor='pointer'
          shape.addEventListener('click',(el)=>{
            // console.log(item.url);
            // window.location.href=item.url;
            console.log('navigate',item.url);
            navigate(item.url)
          })
        }
      });
      target.current.appendChild(clone)
      el.element.style.display = 'none';
    }, 100);
  }

  // useEffect(() => {
  //   const main = document.querySelector('dxdi-main');
  //   console.log(main);
  // },[]);

  return (<div>
    <Diagram readOnly={true} showGrid={false} simpleView={true} onItemClick={navigate} autoZoomMode="fitWidth" onContentReady={fitContent}>
      <Nodes dataSource={flowNodesDataSource} idExpr="id" typeExpr="type" textExpr="text" leftExpr="left" topExpr="top" heightExpr="height" widthExpr="width" styleExpr={style} textStyleExpr={styleText}>
        <AutoLayout type="auto"/>
      </Nodes>
      <Edges dataSource={flowEdgesDataSource} idExpr="id" textExpr="text" fromExpr="fromId" toExpr="toId" toPointIndexExpr="toPointIndex" fromPointIndexExpr="fromPointIndex" lineTypeExpr="type" toLineEndExpr="toLineEnd" fromLineEndExpr="fromLineEnd"/>
      <ViewToolbar visible={false}/>
      <ContextToolbox enable={false}/>
    </Diagram>
    <div ref={target} />
    <Helmet>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.common.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.light.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx-diagram.css"/>
      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
    </Helmet>
  </div>)
}
export default Diag
